# Zork Quest: Assault on Egreth Castle

This is a text-based game based on Zork Quest: Assault on Egreth Castle that I
wrote for my 1st Year Computer Architecture final project (as part of a BSc (Hons)
in Games Development). It was written in 68000 assembly using Easy68k.

* [Download Easy68k](http://www.easy68k.com)
